# margopolo
![margopolo](https://i.imgur.com/ZX2z5w3.jpg)

## Problem Statement
I have a lot of rasperry pis connected to various pieces of hardware such as kinects, 
MIDI controllers, etc.  When I go places with them and connect to other wireless 
networks, I don't know what IP addresses they have.  I wanted a simple way
to find them on other networks.

## Solution
margopolo - a simple service that emits multicast UDP beacons with a topic
and ip address, and a simple command line tool that receives the beacons
and prints the information to stdout.

## Installation
### Dependencies
#### [libsodium](https://github.com/jedisct1/libsodium)
Version: 1.0.2 (or newer)

Sodium is a "new, easy-to-use software library for encryption, decryption, signatures, password hashing and more".  ZeroMQ uses sodium for the basis of the CurveZMQ security protocol.

```
git clone git@github.com:jedisct1/libsodium.git
cd libsodium
./autogen.sh; ./configure; make; make check
sudo make install
sudo ldconfig
```

#### [ZeroMQ](http://zeromq.org/) 
Version: commit 6b4d9bca0c31fc8131749396fd996d17761c999f or newer

ZeroMQ is an embeddable [ZMTP](http://rfc.zeromq.org/spec:23) protocol library.

```
git clone git@github.com:zeromq/libzmq.git
cd libzmq
./autogen.sh; ./configure --with-libsodium; make; make check
sudo make install
sudo ldconfig
```

#### [CZMQ](http://czmq.zeromq.org/)
Version: commit 7997d86bcac7a916535338e71f2d826a9913df28 or newer

CZMQ is a high-level C binding for ZeroMQ.  It provides an API for various services on top of ZeroMQ such as authentication, actors, service discovery, etc.

```
git clone git@github.com:zeromq/czmq.git
cd czmq
./autogen.sh; ./configure; make; make check
sudo make install
sudo ldconfig
```

#### [GoCZMQ](http://https://github.com/zeromq/goczmq)
Version: commit 6b4d9bca0c31fc8131749396fd996d17761c999f or newer

GoCZMQ is a Go interface to the CZMQ API.

```
mkdir -p  $GOPATH/go/src/github.com/zeromq
cd $GOPATH/go/src/github.com/zeromq
git clone git@github.com:zeromq/goczmq.git
cd goczmq
go test
go build; go install
```

### [margopolo](https://github.com/taotetek/margopolo)
```
go get github.com/taotetek/margopolo
cd $GOPATH/src/github.com/taotetek/margopolo/cmd/margo
go build; go install
cd $GOPATH/src/github.com/taotetek/margopolo/cmd/polo
go build; go install
```

## Usage
### Margo
```
$  margo -h
Usage of margo:
  -broadcast interval=500: timeout in milliseconds
  -port=9999: port to broadcast on
  -topic="marco": topic to broadcast on
  -verbose=false: verbose mode

```
```
 margo -verbose
I: 15-05-30 16:06:28 zbeacon: API command=CONFIGURE
I: 15-05-30 16:06:28 zbeacon: interface=wlan0 address=192.168.1.105 broadcast=192.168.1.255
I: 15-05-30 16:06:28 zbeacon: configured, hostname=192.168.1.105
I: 15-05-30 16:06:28 zbeacon: API command=PUBLISH
```
### Polo
```
$  polo -h
Usage of polo:
  -loop=false: continuous mode
  -port=9999: port to listen on
  -timeout=1000: timeout in milliseconds
  -topic="marco": topic to listen on
  -verbose=false: verbose mode
```
```
$  polo -verbose -loop
I: 15-05-30 16:07:30 zbeacon: API command=CONFIGURE
I: 15-05-30 16:07:30 zbeacon: interface=wlan0 address=192.168.1.105 broadcast=192.168.1.255
I: 15-05-30 16:07:30 zbeacon: configured, hostname=192.168.1.105
I: 15-05-30 16:07:30 zbeacon: API command=SUBSCRIBE
```
