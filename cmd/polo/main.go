package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"

	"github.com/zeromq/goczmq"
)

type bmsg struct {
	Address string
	Message string
}

func main() {
	portPtr := flag.Int("port", 9999, "port to listen on")
	topicPtr := flag.String("topic", "marco", "topic to listen on")
	verbosePtr := flag.Bool("verbose", false, "verbose mode")
	timeoutPtr := flag.Int("timeout", 1000, "timeout in milliseconds")
	loopPtr := flag.Bool("loop", false, "continuous mode")
	flag.Parse()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)
	bmsgChan := make(chan bmsg)
	polo := goczmq.NewBeacon()

	go func() {
		if *verbosePtr {
			err := polo.Verbose()
			if err != nil {
				log.Fatalf("could not set verbose: %s", err)
			}
		}

		_, err := polo.Configure(*portPtr)
		if err != nil {
			log.Fatalf("could not start polo on port %d: %s", *portPtr, err)
		}

		polo.Subscribe(*topicPtr)

		for {
			msg := bmsg{}
			msg.Message = polo.Recv(*timeoutPtr)
			msg.Address = polo.Recv(*timeoutPtr)
			bmsgChan <- msg
			if !*loopPtr {
				break
			}
		}
	}()

	for {
		select {
		case <-c:
			goto Exit
		case msg := <-bmsgChan:
			fmt.Printf("%s:%s\n", msg.Address, msg.Message)
			if !*loopPtr {
				goto Exit
			}
		}
	}

Exit:
	polo.Destroy()
}
