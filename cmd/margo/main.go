package main

import (
	"flag"
	"log"
	"os"
	"os/signal"

	"github.com/zeromq/goczmq"
)

func main() {
	portPtr := flag.Int("port", 9999, "port to broadcast on")
	topicPtr := flag.String("topic", "marco", "topic to broadcast on")
	verbosePtr := flag.Bool("verbose", false, "verbose mode")
	intervalPtr := flag.Int("broadcast interval", 500, "timeout in milliseconds")
	flag.Parse()

	marco := goczmq.NewBeacon()

	if *verbosePtr {
		err := marco.Verbose()
		if err != nil {
			log.Fatalf("could not set verbose: %s", err)
		}
	}

	_, err := marco.Configure(*portPtr)
	if err != nil {
		log.Fatalf("could not start marco on port %d: %s", *portPtr, err)
	}

	marco.Publish(*topicPtr, *intervalPtr)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)
	<-c

	marco.Destroy()
}
